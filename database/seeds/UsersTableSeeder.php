<?php

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        User::create(array(
            'email' => 'hamad@gmail.com',
            'name' => 'Hamad',
            'university' => 'King Saud University',
            'specialization' => 'Software Engineering',
            'password' => Hash::make('123456')
        ));

        User::create(array(
            'email' => 'hazzaa@gmail.com',
            'name' => 'Hazzaa',
            'university' => 'King Saud University',
            'specialization' => 'Software Engineering',
            'password' => Hash::make('123456')
        ));

        User::create(array(
            'email' => 'ali@gmail.com',
            'name' => 'Ali',
            'university' => 'King Saud University',
            'specialization' => 'Software Engineering',
            'password' => Hash::make('123456')
        ));

    }

}