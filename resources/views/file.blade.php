@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- USER -->
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align: center">
                        <h4 style="text-align: left;"><b>Uploaded By</b></h4>
                        <hr>
                        <div class="panel panel-default" style="text-align: center">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url(\App\User::find($file->user_id)->avatar_name) }}" alt="profile image" class="img-circle" width="50px" style="border: 2px solid #CCC;margin: 4px auto;">
                                </div>
                                <div class="col-md-12">
                                    <h5><a href="/files/{{$file->user_id}}">{{\App\User::find($file->user_id)->name}}</a></h5>
                                    <h6>{{\App\User::find($file->user_id)->university}}</h6>
                                    <h6>{{\App\User::find($file->user_id)->specialization}}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- UPLOAD FILE -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4><b>Upload File</b></h4>
                        <hr>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul style="padding-left: 15px;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form method="POST" action="{{ url('/files') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="desc">Description : </label>
                                <textarea class="form-control" rows="3" id="desc" name="desc" placeholder="You have 160 characters"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="sfile">Choose a file : </label>
                                <input type="file" id="sfile" name="sfile" accept="application/pdf,application/msword,image/png,image/jpeg">
                                <p class="help-block">pdf , word , jpg , png</p>
                                <input class="btn btn-default btn-block btn-primary" type="submit" value="Upload">
                            </div>
                        </form>
                    </div>
                </div>
                <footer style="text-align: center">
                    Copyright &copy; 2017
                </footer>
            </div>
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>File : <small>{{$file->original_file_name}}</small></h1>
                        <hr>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h4>Description : </h4>
                                        <p>{{$file->description}}</p>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="panel panel-default" style="text-align: center">
                                            <div class="panel-body">
                                                <i class="fa fa-download fa-lg" aria-hidden="true"></i>
                                                <h4 style="margin: 2px;">{{$file->downloads}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="panel panel-default" style="text-align: center">
                                            <div class="panel-body">
                                                <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                                <h4 style="margin: 2px;">{{$file->views}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="list-group">
                                <li class="list-group-item">File type :
                                    @if($file->mime == 'application/pdf')
                                        <span class="label label-danger" style="margin-left: 5px;">PDF</span>
                                    @elseif($file->mime == 'application/msword')
                                        <span class="label label-primary" style="margin-left: 5px;">WORD</span>
                                    @elseif($file->mime == 'image/png' || $file->mime == 'image/jpeg')
                                        <span class="label label-warning" style="margin-left: 5px;">IMAGE</span>
                                    @endif
                                </li>
                                <li class="list-group-item">Uploaded At : <span class="label label-default" style="margin-left: 5px;">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $file->created_at)->toFormattedDateString()}}</span></li>
                            </ul>
                        </div>
                        <hr>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form method="POST" action="/files/download/{{$file->id}}" style="display: inline-block">
                                    {{csrf_field()}}
                                    <button type="submit" class="btn btn-primary">Download</button>
                                </form>
                                <button class="btn btn-info" data-toggle="modal" data-target="#shareModal">Share</button>
                                <!-- Modal -->
                                <div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Share File</h4>
                                            </div>
                                            <div class="modal-body">
                                                <h3>Share via Twitter : </h3>
                                                <a class="twitter-share-button"
                                                   href="https://twitter.com/intent/tweet?text=I%20shared%20this%20file%20from%20StudentBook"
                                                   data-size="large">
                                                    Tweet</a>
                                                <h3>Share via Facebook : </h3>
                                                <div class="fb-share-button" data-href="http://aqueous-sea-47446.herokuapp.com/files/{{$file->id}}" data-layout="button" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">مشاركة</a></div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if(Auth::user()->id == $file->user_id)
                                    <form method="POST" action="/files/{{$file->id}}" style="display: inline-block">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button class="btn btn-danger" type="submit">Remove</button>
                                    </form>
                                    <button class="btn btn-warning" type="submit" data-toggle="modal" data-target="#editFileModal">Edit</button>
                                    <!-- Modal -->
                                    <div class="modal fade bs-example-modal-sm" id="editFileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Edit File Details</h4>
                                                </div>
                                                <form method="POST" action="/files/{{$file->id}}" class="form-horizontal">
                                                <div class="modal-body">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="PATCH">
                                                        <div class="form-group">
                                                            <label for="file_name" class="col-sm-3 control-label">File Name</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" id="file_name" name="file_name" placeholder="File Name" value="{{$file->original_file_name}}">
                                                            </div>
                                                            <br><br><br>
                                                            <label for="desc" class="col-sm-3 control-label">File Description</label>
                                                            <div class="col-sm-9">
                                                                <textarea class="form-control" rows="3" id="desc" name="desc" placeholder="You have 160 characters">{{$file->description}}</textarea>
                                                            </div>
                                                            <br>
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
