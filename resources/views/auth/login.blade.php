@extends('layouts.app')

@section('content')
    <!-- LOGIN FORM -->
    <div class="text-center" style="padding:50px 0">
        <div class="login-container">
            <div id="output">
                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
            </div>
            <h2>StudentBook</h2>
            <br>
            <div class="form-box">
                <form id="login-form" class="text-left" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}
                    <input type="text" class="form-control{{ $errors->has('email') ? ' has-error' : '' }}" id="email" name="email" placeholder="email address" value="">
                    <input type="password" class="form-control{{ $errors->has('password') ? ' has-error' : '' }}" id="password" name="password" placeholder="password">
                    <button class="btn btn-primary btn-block login" type="submit">Login</button>
                </form>
            </div>
        </div>
    </div>
@endsection