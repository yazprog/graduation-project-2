@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align: center">
                        <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url($current_user->avatar_name) }}" alt="profile image" class="img-circle" width="200px" style="border: 4px solid #CCC;margin: 15px auto;">
                        <h4>{{$current_user->name}}</h4>
                        <h6>{{$current_user->university}}</h6>
                        <h6>{{$current_user->specialization}}</h6>
                        @if(Auth::user()->id != $current_user->id && !$he_is_friends)
                        <form method="POST" action="{{ url('/friends') }}">
                            {{csrf_field()}}
                            <input type="hidden" name="friend_id" value="{{$current_user->id}}">
                            <button class="btn btn-xs btn-primary" type="submit">Add Friend</button>
                        </form>
                        @endif
                    </div>
                    <!-- List group -->
                    <ul class="list-group">
                        <il class="list-group-item"><i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;"></i>
                            <span class="badge">{{$current_user->friends->count()}}</span>
                            Following
                        </il>
                        <il class="list-group-item">
                            <i class="fa fa-file" aria-hidden="true" style="margin-right: 10px;"></i>
                            <span class="badge">{{$current_user->files->count()}}</span>
                            Files
                        </il>
                        <il class="list-group-item">
                            <i class="fa fa-question-circle" aria-hidden="true" style="margin-right: 10px;"></i>
                            <span class="badge">{{$current_user->questions->count()}}</span>
                            Questions
                        </il>
                        @if(Auth::user()->id == $current_user->id)
                        <a href="/groups" class="list-group-item"><i class="fa fa-users" aria-hidden="true" style="margin-right: 10px;"></i>
                            <span class="badge">{{$current_user->groups->count()}}</span>
                            Groups
                        </a>
                        <a href="/rooms" class="list-group-item"><i class="fa fa-comments" aria-hidden="true" style="margin-right: 10px;"></i>
                            <span class="badge">{{$current_user->rooms->count()}}</span>
                            Chat Rooms
                        </a>
                        @endif
                    </ul>
                </div>
                <footer style="text-align: center">
                    Copyright &copy; 2017
                </footer>
            </div>
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <!-- -->
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#files" aria-controls="home" role="tab" data-toggle="tab">Files</a></li>
                                    <li role="presentation"><a href="#questions" aria-controls="views" role="tab" data-toggle="tab">Questions</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <!-- -->
                                    <div role="tabpanel" class="tab-pane active" id="files">
                                        <br>
                                        <ul class="list-group">
                                            @forelse($current_user->files->sortByDesc('created_at') as $file)
                                                <li class="list-group-item">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <a href="/files/{{$file->id}}" style="font-size: 20px;">{{$file->original_file_name}}</a>
                                                            <p>{{$file->description}}</p>
                                                            <span class="label label-default">{{\App\User::find($file->user_id)->name}}</span>
                                                            @if($file->mime == 'application/pdf')
                                                                <span class="label label-danger">PDF</span>
                                                            @elseif($file->mime == 'application/msword')
                                                                <span class="label label-primary">WORD</span>
                                                            @elseif($file->mime == 'image/png' || $file->mime == 'image/jpeg')
                                                                <span class="label label-warning">IMAGE</span>
                                                            @endif
                                                            <small style="margin-left: 7px;">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $file->created_at)->diffForHumans()  }}</small>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="panel panel-default" style="text-align: center">
                                                                <div class="panel-body">
                                                                    <i class="fa fa-download fa-lg" aria-hidden="true"></i>
                                                                    <h4 style="margin: 2px;">{{$file->downloads}}</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="panel panel-default" style="text-align: center">
                                                                <div class="panel-body">
                                                                    <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                                                    <h4 style="margin: 2px;">{{$file->views}}</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @empty
                                                <h1>No Files</h1>
                                            @endforelse
                                        </ul>
                                    </div>
                                    <!-- -->
                                    <div role="tabpanel" class="tab-pane" id="questions">
                                        <br>
                                        <ul class="list-group">
                                            @forelse($current_user->questions->sortByDesc('created_at') as $question)
                                                <li class="list-group-item">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <a href="/questions/{{$question->id}}" style="font-size: 20px;">{{$question->title}}</a>
                                                            <br>
                                                            <span class="label label-default">{{\App\User::find($question->user_id)->name}}</span>
                                                            <small style="margin-left: 7px;">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $question->created_at)->diffForHumans()  }}</small>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="panel panel-default" style="text-align: center">
                                                                <div class="panel-body">
                                                                    @if($question->answers->count() > 0)
                                                                        <i class="fa fa-lightbulb-o fa-lg" aria-hidden="true"></i>
                                                                        <h4 style="margin: 2px;">{{$question->answers->count()}}</h4>
                                                                    @else
                                                                        <i class="fa fa-lightbulb-o fa-lg" aria-hidden="true"></i>
                                                                        <h4 style="margin: 2px;">0</h4>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="panel panel-default" style="text-align: center">
                                                                <div class="panel-body">
                                                                    <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                                                    <h4 style="margin: 2px;">{{$question->views}}</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @empty
                                                <h1>No Questions</h1>
                                            @endforelse
                                        </ul>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
