<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>StudentBook</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">
    <link rel="stylesheet" href="/css/chat.css">

    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://cdn.rawgit.com/samsonjs/strftime/master/strftime-min.js"></script>
    <script src="//js.pusher.com/3.0/pusher.min.js"></script>

    <script>
        // Ensure CSRF token is sent with AJAX requests
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Added Pusher logging
        Pusher.log = function(msg) {
            console.log(msg);
        };
    </script>

    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
            padding-top: 70px;
        }

        .fa-btn {
            margin-right: 6px;
        }

        div.panel{
            border: 1.5px solid #CCC;
        }
    </style>
</head>

<body id="app-layout" style="background-color: #ecf0f1">

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    StudentBook
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    @if (!Auth::guest())
                        <li><a href="{{ url('/home') }}">Home</a></li>
                        <li><a href="{{ url('/files') }}">Files</a></li>
                        <li><a href="{{ url('/questions') }}">Q&A</a></li>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }}
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/me') }}"><i class="fa fa-btn fa-user"></i>Me</a></li>
                                <li><a href="{{ url('/settings') }}"><i class="fa fa-btn fa-cog"></i>Setting</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-bell" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>No Thing</li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- USER -->
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align: center">
                        <div class="col-md-4">
                            <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url($current_user->avatar_name) }}" alt="profile image" class="img-circle" width="100%" style="border: 2px solid #CCC;margin: 5px auto;">
                        </div>
                        <div class="col-md-8">
                            <h5>{{$current_user->name}}</h5>
                            <h6>{{$current_user->specialization}}</h6>
                        </div>
                    </div>
                </div>
                <!-- -->
                @if(\Illuminate\Support\Facades\Auth::user()->id == $room->user_id)
                <div class="panel panel-default">
                    <div class="panel-body">
                        @if(\App\Group::find($room->group_id)->members->count() != $room->invitations->count())
                            <form method="POST" action="{{ url('/rooms/invitations') }}">
                                {{csrf_field()}}
                                <input type="hidden" name="room_id" value="{{$room->id}}">
                                <input type="hidden" name="group_id" value="{{$room->group_id}}">
                                <button class="btn btn-lg btn-block btn-primary" type="submit">Send Invitations</button>
                            </form>
                        @else
                            <button class="btn btn-lg btn-block btn-primary" type="submit" disabled>Send Invitations</button>
                        @endif
                    </div>
                </div>
                @endif
                <!-- NEW ROOM -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4><b>Group Members</b></h4>
                    </div>
                    <!-- List group -->
                    <ul class="list-group">
                        @if(\Illuminate\Support\Facades\Auth::user()->id == $room->user_id)
                            @foreach(\App\Group::find($room->group_id)->members as $member)
                            <li class="list-group-item">
                                @if(\App\Invitation::where('user_id','=',$current_user->id)->where('receiver_id',$member->id)->where('room_id',$room->id)->get()->isEmpty())
                                    <span class="pull-right">Not Invited</span>
                                @else
                                    <span class="pull-right">Invited</span>
                                @endif
                                {{$member->name}}
                            </li>
                            @endforeach
                        @else
                            @foreach(\App\Group::find($room->group_id)->members as $member)
                                <li class="list-group-item">
                                    {{$member->name}}
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
                <footer style="text-align: center">
                    Copyright &copy; 2017
                </footer>
            </div>
            <!--------------------------------------->
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>Room : {{$room->name}}</h1>
                        <small>{{\App\Group::find($room->group_id)->name}} group</small>
                        <hr>
                        <div class="row">

                            <div class="chat_window">
                                <ul id="messages"></ul>
                                <div class="bottom_wrapper clearfix">
                                    <div class="message_input_wrapper">
                                        <input class="input-message" placeholder="Type your message here..." />
                                    </div>
                                    <div class="send-message">
                                        <div class="icon"></div>
                                        <div class="text">Send</div>
                                    </div>
                                </div>
                            </div>
                            <div class="message_template">
                                <li class="message left">
                                    <div class="avatar">
                                        <img src="">
                                    </div>
                                    <div class="text_wrapper">
                                        <div class="text message-body"></div>
                                        <span class="author"></span>
                                        <span class="timestamp"></span>
                                        <span class="seen"></span>
                                    </div>
                                </li>
                            </div>

                            <script id="chat_message_template" type="text/template">
                                <li class="message right">
                                    <div class="avatar">
                                        <img src="">
                                    </div>
                                    <div class="text_wrapper">
                                        <div class="text message-body"></div>
                                        <span class="author"></span>
                                        <span class="timestamp"></span>
                                        <span class="seen"></span>
                                    </div>
                                </li>
                            </script>

                            <script>
                            function init() {
                                // send button click handling
                                $('.send-message').click(sendMessage);
                                $('.input-message').keypress(checkSend);
                            }

                            // Send on enter/return key
                            function checkSend(e) {
                                if (e.keyCode === 13) {
                                    return sendMessage();
                                }
                            }

                            // Handle the send button being clicked
                            function sendMessage() {
                                var messageText = $('.input-message').val();
                                if(messageText.length < 3) {
                                    return false;
                                }

                                // Build POST data and make AJAX request
                                var data = {chat_text: messageText};
                                $.post('/rooms/chat/message', data).success(sendMessageSuccess);

                                // Ensure the normal browser event doesn't take place
                                return false;
                            }

                            // Handle the success callback
                            function sendMessageSuccess() {
                                $('.input-message').val('');
                                console.log('message sent successfully');
                            }

                            // Build the UI for a new message and add to the DOM
                            function addMessage(data) {
                                // Create element from template and set values
                                var el = createMessageEl();
                                el.find('.message-body').html(data.text);
                                el.find('.author').text(data.name);
                                el.find('.avatar img').attr('src','https://s3-us-west-2.amazonaws.com/graduation-projects-2/'+data.avatar);

                                // Utility to build nicely formatted time
                                el.find('.timestamp').text(strftime('%H:%M:%S %P', new Date(data.timestamp)));

                                var messages = $('#messages');
                                messages.append(el)

                                // Make sure the incoming message is shown
                                messages.scrollTop(messages[0].scrollHeight);
                            }

                            // Creates an activity element from the template
                            function createMessageEl() {
                                var text = $('#chat_message_template').text();
                                var el = $(text);
                                return el;
                            }

                            $(init);

                            /***********************************************/

                            var pusher = new Pusher('52116eaf6365325a2735');

                            var channel = pusher.subscribe('{{$chatChannel}}');
                            channel.bind('new-message', addMessage);

                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/chat.js"></script>
    <script>window.twttr = (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0],
                    t = window.twttr || {};
            if (d.getElementById(id)) return t;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);

            t._e = [];
            t.ready = function(f) {
                t._e.push(f);
            };

            return t;
        }(document, "script", "twitter-wjs"));</script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>

