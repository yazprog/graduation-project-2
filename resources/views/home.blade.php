@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body" style="text-align: center">
                    <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url($current_user->avatar_name) }}" alt="profile image" class="img-circle" width="200px" style="border: 4px solid #CCC;margin: 15px auto;">
                    <h4>{{$current_user->name}}</h4>
                    <h6>{{$current_user->university}}</h6>
                    <h6>{{$current_user->specialization}}</h6>
                </div>
                <!-- List group -->
                <ul class="list-group">
                    <a href="/friends" class="list-group-item"><i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;"></i>
                        <span class="badge">{{$current_user->friends->count()}}</span>
                        Following
                    </a>
                    <a href="/groups" class="list-group-item"><i class="fa fa-users" aria-hidden="true" style="margin-right: 10px;"></i>
                        <span class="badge">{{$current_user->groups->count()}}</span>
                        Groups
                    </a>
                    <a href="/rooms" class="list-group-item"><i class="fa fa-comments" aria-hidden="true" style="margin-right: 10px;"></i>
                        <span class="badge">{{$current_user->rooms->count() + \App\Invitation::where('receiver_id',$current_user->id)->where('state',true)->count()}}</span>
                        Chat Rooms
                    </a>
                </ul>
            </div>
            <footer style="text-align: center">
                Copyright &copy; 2017
            </footer>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h1>Activity Stream</h1>
                    <hr>
                    <!-- -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3>Me <small>last week</small></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Files</h3>
                                        </div>
                                        <ul class="list-group">
                                        @if(!$my_files->isEmpty())
                                            @foreach($my_files->sortByDesc('created_at') as $file)
                                                <li class="list-group-item">
                                                    <a href="/files/{{$file->id}}"><b>{{$file->original_file_name}}</b></a>
                                                    <div class="pull-right">
                                                        <span class="badge"><i class="fa fa-download" aria-hidden="true"></i> <span style="margin-left: 5px;">{{$file->downloads}}</span> </span>
                                                        <span class="badge"><i class="fa fa-eye" aria-hidden="true"></i>  <span style="margin-left: 5px;">{{$file->views}}</span> </span>
                                                    </div>
                                                    <br>
                                                    <span class="label label-default">Me</span>
                                                    <small>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $file->created_at)->diffForHumans()  }}</small>
                                                </li>
                                            @endforeach
                                        @else
                                                <li class="list-group-item">No Files</li>
                                        @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Q&A</h3>
                                        </div>
                                        <ul class="list-group">
                                            @if(!$my_questions->isEmpty())
                                                @foreach($my_questions->sortByDesc('created_at') as $question)
                                                    <li class="list-group-item">
                                                        <a href="/questions/{{$question->id}}"><b>{{$question->title}}</b></a>
                                                        <div class="pull-right">
                                                            @if($question->answers->count() > 0)
                                                                <span class="badge"><i class="fa fa-lightbulb-o" aria-hidden="true"></i> <span style="margin-left: 5px;">{{$question->answers->count()}}</span> </span>
                                                            @else
                                                                <span class="badge"><i class="fa fa-lightbulb-o" aria-hidden="true"></i> <span style="margin-left: 5px;">0</span> </span>
                                                            @endif
                                                            <span class="badge"><i class="fa fa-eye" aria-hidden="true"></i>  <span style="margin-left: 5px;">{{$question->views}}</span> </span>
                                                        </div>
                                                        <br>
                                                        <span class="label label-default">Me</span>
                                                        <small>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $question->created_at)->diffForHumans()  }}</small>
                                                    </li>
                                                @endforeach
                                            @else
                                                <li class="list-group-item">No Questions and Answers</li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3>My Following </h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Files</h3>
                                        </div>
                                        <ul class="list-group">
                                            @if(!$friends_files->isEmpty())
                                                @foreach($friends_files->sortByDesc('created_at') as $file)
                                                    <li class="list-group-item">
                                                        <a href="/files/{{$file->id}}"><b>{{$file->original_file_name}}</b></a>
                                                        <div class="pull-right">
                                                            <span class="badge"><i class="fa fa-download" aria-hidden="true"></i> <span style="margin-left: 5px;">{{$file->downloads}}</span> </span>
                                                            <span class="badge"><i class="fa fa-eye" aria-hidden="true"></i>  <span style="margin-left: 5px;">{{$file->views}}</span> </span>
                                                        </div>
                                                        <br>
                                                        <span class="label label-default">{{\App\User::find($file->user_id)->name}}</span>
                                                        <small>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $file->created_at)->diffForHumans()  }}</small>
                                                    </li>
                                                @endforeach
                                            @else
                                                <li class="list-group-item">No Files</li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Q&A</h3>
                                        </div>
                                        <ul class="list-group">
                                            @if(!$friends_questions->isEmpty())
                                            @foreach($friends_questions->sortByDesc('created_at') as $question)
                                                <li class="list-group-item">
                                                    <a href="/questions/{{$question->id}}"><b>{{$question->title}}</b></a>
                                                    <div class="pull-right">
                                                        @if($question->answers->count() > 0)
                                                            <span class="badge"><i class="fa fa-lightbulb-o" aria-hidden="true"></i> <span style="margin-left: 5px;">{{$question->answers->count()}}</span> </span>
                                                        @else
                                                            <span class="badge"><i class="fa fa-lightbulb-o" aria-hidden="true"></i> <span style="margin-left: 5px;">0</span> </span>
                                                        @endif
                                                        <span class="badge"><i class="fa fa-eye" aria-hidden="true"></i>  <span style="margin-left: 5px;">{{$question->views}}</span> </span>
                                                    </div>
                                                    <br>
                                                    <span class="label label-default">{{\App\User::find($question->user_id)->name}}</span>
                                                    <small>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $question->created_at)->diffForHumans()  }}</small>
                                                </li>
                                            @endforeach
                                            @else
                                                <li class="list-group-item">No Questions and Answers</li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
