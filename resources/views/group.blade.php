@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- USER -->
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align: center">
                        <div class="col-md-4">
                            <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url($current_user->avatar_name) }}" alt="profile image" class="img-circle" width="100%" style="border: 2px solid #CCC;margin: 5px auto;">
                        </div>
                        <div class="col-md-8">
                            <h5>{{$current_user->name}}</h5>
                            <h6>{{$current_user->specialization}}</h6>
                        </div>
                    </div>
                </div>
                <!-- UPLOAD FILE -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4><b>Add Member</b></h4>
                        <hr>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul style="padding-left: 15px;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form rele="form" method="POST" action="{{ url('/groups/member')  }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="group_id" value="{{$group->id}}">
                            <div class="form-group">
                                <label for="name">Choose Member</label>
                                <select class="form-control" name="member_id" id="member_id">
                                    <option disabled selected>Your Friends</option>
                                    @foreach($current_user->friends as $friend)
                                        <option value="{{$friend->id}}">{{$friend->name}}</option>
                                    @endforeach
                                </select>
                                <br>
                                <input class="btn btn-primary btn-block" type="submit" value="Save">
                            </div>
                        </form>
                    </div>
                </div>
                <footer style="text-align: center">
                    Copyright &copy; 2017
                </footer>
            </div>
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h1>{{$group->name}}</h1>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Members</h3>
                                    </div>
                                    <ul class="list-group">
                                        @forelse($group->members as $member)
                                            <li class="list-group-item">
                                                <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url($member->avatar_name) }}" class="img-circle" width="40px" style="margin-right: 10px;border: 2px solid #CCC;">
                                                <a href="/profile/{{$member->id}}">{{$member->name}}</a>
                                                <div class="pull-right">
                                                    <form rele="form" method="POST" action="{{ url('/groups/member/'.$member->id)  }}" style="display: inline-block;">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <input type="hidden" name="group_id" value="{{$group->id}}">
                                                        <button type="submit" class="btn btn-danger btn-xs">Remove</button>
                                                    </form>
                                                </div>
                                            </li>
                                        @empty
                                            <h1>No Members</h1>
                                        @endforelse
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
