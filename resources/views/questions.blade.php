@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- USER -->
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align: center">
                        <div class="col-md-4">
                            <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url($current_user->avatar_name) }}" alt="profile image" class="img-circle" width="100%" style="border: 2px solid #CCC;margin: 5px auto;">
                        </div>
                        <div class="col-md-8">
                            <h5>{{$current_user->name}}</h5>
                            <h6>{{$current_user->specialization}}</h6>
                        </div>
                    </div>
                </div>
                <!-- SEARCH -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form method="POST" action="{{ url('/questions/search') }}">
                            {{ csrf_field() }}
                            <div class="input-group">
                                <input type="text" name="title" class="form-control" placeholder="Search for Question ...">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-default btn-primary" type="button">Go!</button>
                                 </span>
                            </div><!-- /input-group -->
                        </form>
                    </div>
                </div>
                <!-- ASK QUESTION -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4><b>Ask Question</b></h4>
                        <hr>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul style="padding-left: 15px;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form method="POST" action="{{ url('/questions') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="desc">Title : </label>
                                <input type="text" class="form-control" id="title" name="title" required>
                            </div>
                            <div class="form-group">
                                <label for="desc">Your Question : </label>
                                <textarea class="form-control" rows="3" id="question" name="question"></textarea>
                            </div>
                            <input class="btn btn-default btn-block btn-primary" type="submit" value="Ask">
                        </form>
                    </div>
                </div>
                <footer style="text-align: center">
                    Copyright &copy; 2017
                </footer>
            </div>
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>Q&A</h1>
                        <hr>
                        <!-- -->
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#views" aria-controls="views" role="tab" data-toggle="tab">Views</a></li>
                                    <li role="presentation"><a href="#week" aria-controls="week" role="tab" data-toggle="tab">Week</a></li>
                                    <li role="presentation"><a href="#month" aria-controls="month" role="tab" data-toggle="tab">Month</a></li>
                                </ul>
                                <div class="tab-content">
                                    <!-- Views -->
                                    <div role="tabpanel" class="tab-pane active" id="views">
                                        <br>
                                        <ul class="list-group">
                                            @foreach(\App\Question::all()->sortByDesc('views') as $question)
                                                <li class="list-group-item">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <a href="/questions/{{$question->id}}" style="font-size: 20px;">{{$question->title}}</a>
                                                            <br>
                                                            <span class="label label-default">{{\App\User::find($question->user_id)->name}}</span>
                                                            <small style="margin-left: 7px;">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $question->created_at)->diffForHumans()  }}</small>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="panel panel-default" style="text-align: center">
                                                                <div class="panel-body">
                                                                    @if($question->answers->count() > 0)
                                                                        <h4 style="margin: 2px;"><i class="fa fa-lightbulb-o fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>{{$question->answers->count()}}</h4>
                                                                    @else
                                                                        <h4 style="margin: 2px;"><i class="fa fa-lightbulb-o fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>0</h4>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="panel panel-default" style="text-align: center">
                                                                <div class="panel-body">
                                                                    <h4 style="margin: 2px;"><i class="fa fa-eye fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>{{$question->views}}</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <!-- Week -->
                                    <div role="tabpanel" class="tab-pane" id="week">
                                        <br>
                                        <ul class="list-group">
                                            @foreach(\App\Question::where('created_at','>=',\Carbon\Carbon::now()->subWeek()->toDateTimeString())->get()->sortByDesc('created_at') as $question)
                                                <li class="list-group-item">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <a href="/questions/{{$question->id}}" style="font-size: 20px;">{{$question->title}}</a>
                                                            <br>
                                                            <span class="label label-default">{{\App\User::find($question->user_id)->name}}</span>
                                                            <small style="margin-left: 7px;">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $question->created_at)->diffForHumans()  }}</small>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="panel panel-default" style="text-align: center">
                                                                <div class="panel-body">
                                                                    @if($question->answers->count() > 0)
                                                                        <h4 style="margin: 2px;"><i class="fa fa-lightbulb-o fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>{{$question->answers->count()}}</h4>
                                                                    @else
                                                                        <h4 style="margin: 2px;"><i class="fa fa-lightbulb-o fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>0</h4>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="panel panel-default" style="text-align: center">
                                                                <div class="panel-body">
                                                                    <h4 style="margin: 2px;"><i class="fa fa-eye fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>{{$question->views}}</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <!-- Month -->
                                    <div role="tabpanel" class="tab-pane" id="month">
                                        <br>
                                        <ul class="list-group">
                                            @foreach(\App\Question::where('created_at','>=',\Carbon\Carbon::now()->subMonth()->toDateTimeString())->get()->sortByDesc('created_at') as $question)
                                                <li class="list-group-item">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <a href="/questions/{{$question->id}}" style="font-size: 20px;">{{$question->title}}</a>
                                                            <br>
                                                            <span class="label label-default">{{\App\User::find($question->user_id)->name}}</span>
                                                            <small style="margin-left: 7px;">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $question->created_at)->diffForHumans()  }}</small>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="panel panel-default" style="text-align: center">
                                                                <div class="panel-body">
                                                                    @if($question->answers->count() > 0)
                                                                        <h4 style="margin: 2px;"><i class="fa fa-lightbulb-o fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>{{$question->answers->count()}}</h4>
                                                                    @else
                                                                        <h4 style="margin: 2px;"><i class="fa fa-lightbulb-o fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>0</h4>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="panel panel-default" style="text-align: center">
                                                                <div class="panel-body">
                                                                    <h4 style="margin: 2px;"><i class="fa fa-eye fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>{{$question->views}}</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

