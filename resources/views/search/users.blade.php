@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- USER -->
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align: center">
                        <div class="col-md-4">
                            <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url($current_user->avatar_name) }}" alt="profile image" class="img-circle" width="100%" style="border: 2px solid #CCC;margin: 5px auto;">
                        </div>
                        <div class="col-md-8">
                            <h5>{{$current_user->name}}</h5>
                            <h6>{{$current_user->specialization}}</h6>
                        </div>
                    </div>
                </div>
                <!-- SEARCH -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form method="POST" action="{{ url('/users/search') }}">
                            {{ csrf_field() }}
                            <div class="input-group">
                                <input type="text" name="name" class="form-control" placeholder="Search for User ...">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-default btn-primary" type="button">Go!</button>
                                 </span>
                            </div><!-- /input-group -->
                        </form>
                    </div>
                </div>
                <footer style="text-align: center">
                    Copyright &copy; 2017
                </footer>
            </div>
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>Search Result</h1>
                        <hr>
                                @if(!$errors->isEmpty())
                                    No Result !
                                @else
                                @foreach($users as $user)
                                    <div class="col-md-4">
                                        <div class="panel panel-default" style="text-align: center">
                                            <div class="panel-body">
                                                <div class="col-md-12">
                                                    <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url($user->avatar_name) }}" alt="profile image" class="img-circle" width="50px" style="border: 2px solid #CCC;margin: 5px auto;">
                                                </div>
                                                <div class="col-md-12">
                                                    <h5>{{$user->name}}</h5>
                                                </div>
                                                <a href="/profile/{{$user->id}}" class="btn btn-xs btn-primary" type="button">User profile</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
