@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- USER -->
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align: center">
                        <div class="col-md-4">
                            <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url($current_user->avatar_name) }}" alt="profile image" class="img-circle" width="100%" style="border: 2px solid #CCC;margin: 5px auto;">
                        </div>
                        <div class="col-md-8">
                            <h5>{{$current_user->name}}</h5>
                            <h6>{{$current_user->specialization}}</h6>
                        </div>
                    </div>
                </div>
                <!-- SEARCH -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form method="POST" action="{{ url('/files/search') }}">
                            {{ csrf_field() }}
                            <div class="input-group">
                                <input type="text" name="name" class="form-control" placeholder="Search for File ...">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-default btn-primary" type="button">Go!</button>
                                 </span>
                            </div><!-- /input-group -->
                        </form>
                    </div>
                </div>
                <footer style="text-align: center">
                    Copyright &copy; 2017
                </footer>
            </div>
            <div class="col-md-9">
                <div class="panel panel-default">
                        <div class="panel-body">
                            <h1>Search Result</h1>
                            <hr>
                                <div class="panel panel-default">
                                    <ul class="list-group">
                                        @if(!$errors->isEmpty())
                                            No Result
                                        @else
                                        @foreach($files as $file)
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <a href="/files/{{$file->id}}" style="font-size: 20px;">{{$file->original_file_name}}</a>
                                                    <p>{{$file->description}}</p>
                                                    <span class="label label-default">{{\App\User::find($file->user_id)->name}}</span>
                                                    @if($file->mime == 'application/pdf')
                                                        <span class="label label-danger">PDF</span>
                                                    @elseif($file->mime == 'application/msword')
                                                        <span class="label label-primary">WORD</span>
                                                    @elseif($file->mime == 'image/png' || $file->mime == 'image/jpeg')
                                                        <span class="label label-warning">IMAGE</span>
                                                    @endif
                                                    <small style="margin-left: 7px;">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $file->created_at)->diffForHumans()  }}</small>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="panel panel-default" style="text-align: center">
                                                        <div class="panel-body">
                                                            <i class="fa fa-download fa-lg" aria-hidden="true"></i>
                                                            <h4 style="margin: 2px;">{{$file->downloads}}</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="panel panel-default" style="text-align: center">
                                                        <div class="panel-body">
                                                            <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                                            <h4 style="margin: 2px;">{{$file->views}}</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                        @endif
                                    </ul>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
