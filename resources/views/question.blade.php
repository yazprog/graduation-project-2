@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- USER -->
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align: center">
                        <h4 style="text-align: left;"><b>Asked By</b></h4>
                        <hr>
                        <div class="panel panel-default" style="text-align: center">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url(\App\User::find($question->user_id)->avatar_name) }}" alt="profile image" class="img-circle" width="50px" style="border: 2px solid #CCC;margin: 4px auto;">
                                </div>
                                <div class="col-md-12">
                                    <h5><a href="/profile/{{$question->user_id}}">{{\App\User::find($question->user_id)->name}}</a></h5>
                                    <h6>{{\App\User::find($question->user_id)->university}}</h6>
                                    <h6>{{\App\User::find($question->user_id)->specialization}}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ANSWER QUESTION -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4><b>Add Answer</b></h4>
                        <hr>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul style="padding-left: 15px;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form method="POST" action="{{ url('/answers') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="question_id" value="{{$question->id}}">
                            <div class="form-group">
                                <label for="desc">Your Answer : </label>
                                <textarea class="form-control" rows="3" id="answer" name="answer"></textarea>
                            </div>
                            <input class="btn btn-default btn-block btn-primary" type="submit" value="Submit">
                        </form>
                    </div>
                </div>
                <footer style="text-align: center">
                    Copyright &copy; 2017
                </footer>
            </div>
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1 class="text-primary">{{$question->title}}</h1>
                        <hr>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h4>The Question : </h4>
                                        <p style="font-size: 15px">{{$question->content}}</p>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="panel panel-default" style="text-align: center">
                                            <div class="panel-body">
                                                <i class="fa fa-lightbulb-o fa-lg" aria-hidden="true"></i>
                                                <h4 style="margin: 2px;">{{$question->answers->count()}}</h4>
                                            </div>
                                        </div>
                                        <div class="panel panel-default" style="text-align: center">
                                            <div class="panel-body">
                                                <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                                <h4 style="margin: 2px;">{{$question->views}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if(Auth::user()->id == $question->user_id)
                                    <hr>
                                    <form method="POST" action="/questions/{{$question->id}}" style="display: inline-block">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button class="btn btn-danger" type="submit">Remove</button>
                                    </form>
                                    <button class="btn btn-warning" type="submit" data-toggle="modal" data-target="#editQuestionModal">Edit</button>
                                    <!-- Modal -->
                                    <div class="modal fade bs-example-modal-sm" id="editQuestionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Edit Question Details</h4>
                                                </div>
                                                <form method="POST" action="/questions/{{$question->id}}" class="form-horizontal">
                                                    <div class="modal-body">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="PATCH">
                                                        <div class="form-group">
                                                            <label for="title" class="col-sm-3 control-label">Question Title</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" id="title" name="title" placeholder="Question Title" value="{{$question->title}}">
                                                            </div>
                                                            <br><br><br>
                                                            <label for="question" class="col-sm-3 control-label">The Question</label>
                                                            <div class="col-sm-9">
                                                                <textarea class="form-control" rows="3" id="question" name="question" placeholder="">{{$question->content}}</textarea>
                                                            </div>
                                                            <br>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <!-- -->
                        <div class="panel panel-default">
                            <div class="panel-heading">Answers</div>
                            <ul class="list-group">
                                @if(!$question->answers->isEmpty())
                                    @foreach($question->answers as $answer)
                                        <li class="list-group-item">
                                            <div class="media">
                                                <div class="media-left">
                                                    <a href="">
                                                        <img class="media-object img-circle" src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url(\App\User::find($answer->user_id)->avatar_name) }}" alt="..." width="50px" height="50px">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">{{\App\User::find($answer->user_id)->name}}</h4>
                                                    {{$answer->content}}
                                                    <br>
                                                    @if($answer->user_id == $current_user->id)
                                                    <div style="margin-top: 5px;">
                                                        <form method="POST" action="{{ url('/answers/'.$answer->id) }}" style="display: inline-block">
                                                            {{csrf_field()}}
                                                            <input type="hidden" name="_method" value="DELETE">
                                                            <button class="btn btn-danger btn-xs" type="submit">Remove</button>
                                                        </form>
                                                        <small style="margin-left:10px;">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $answer->created_at)->diffForHumans()  }}</small>
                                                    </div>
                                                    @else
                                                        <div style="margin-top: 5px;">
                                                            <small style="margin-left:10px;">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $answer->created_at)->diffForHumans()  }}</small>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                @else
                                    <li class="list-group-item">No Answers</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

