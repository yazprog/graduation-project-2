@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- USER -->
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align: center">
                        <div class="col-md-4">
                            <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url($current_user->avatar_name) }}" alt="profile image" class="img-circle" width="100%" style="border: 2px solid #CCC;margin: 5px auto;">
                        </div>
                        <div class="col-md-8">
                            <h5>{{$current_user->name}}</h5>
                            <h6>{{$current_user->specialization}}</h6>
                        </div>
                    </div>
                </div>
                <!-- NEW GROUP -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4><b>New Group</b></h4>
                        <hr>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul style="padding-left: 15px;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form" rele="form" method="POST" action="{{ url('/groups')  }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="group_name">Group Name</label>
                                <input type="text" class="form-control" id="group_name" name="group_name" required>
                            </div>
                            <div class="form-group">
                                <label for="icon" class="control-label">Group Icon</label>
                                <input type="file" id="icon" name="icon" accept="image/jpeg">
                                <p class="help-block">Only JPG file</p>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-default btn-block btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
                <footer style="text-align: center">
                    Copyright &copy; 2017
                </footer>
            </div>
            <!--------------------------------------->
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>My Groups <small>Subtext for header</small></h1>
                        <hr>
                        <div class="row">
                            @forelse($current_user->groups as $group)
                                <div class="col-md-4">
                                    <div class="panel panel-default">
                                        <div class="panel-body" style="text-align: center">
                                            <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url($group->icon_name) }}" alt="profile image" class="img-circle" width="100px" height="100px" style="border: 4px solid #CCC;margin: 15px auto;">
                                            <h4><a href="/groups/{{$group->id}}">{{$group->name}}</a></h4>
                                            <hr>
                                            <div class="pull-left">
                                                <form class="form-inline" role="form" method="post" action="{{ url('/groups/'.$group->id) }}" style="display: inline-block;">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <button class="btn btn-xs btn-danger" type="submit">Remove</button>
                                                </form>
                                                <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#editGroupModal{{$group->id}}">Edit</button>
                                            </div>
                                            <div class="pull-right">
                                                <span class="badge"><i class="fa fa-user" aria-hidden="true"></i> <span style="margin-left: 5px;">{{$group->members->count()}}</span> </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Edit Modal -->
                                <div class="modal fade" id="editGroupModal{{$group->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Edit Group</h4>
                                            </div>
                                            <form method="POST" action="{{ url('/groups/'.$group->id ) }}" class="form-horizontal" enctype="multipart/form-data">
                                                <div class="modal-body">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="PATCH">
                                                        <div class="form-group">
                                                            <label for="group_name" class="col-sm-3 control-label">Group Name</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" id="group_name" name="group_name" placeholder="Group Name" value="{{$group->name}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="icon" class="col-sm-3 control-label">Group Icon</label>
                                                            <div class="col-sm-9">
                                                                <input type="file" id="icon" name="icon" accept="image/jpeg">
                                                                <p class="help-block">Only JPG file</p>
                                                            </div>

                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-default">Save</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <h1>No Groups</h1>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
