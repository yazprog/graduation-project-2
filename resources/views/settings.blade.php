@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align: center">
                        <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url($current_user->avatar_name) }}" alt="profile image" class="img-circle" width="200px" style="border: 4px solid #CCC;margin: 15px auto;">
                        <h4>{{$current_user->name}}</h4>
                        <h6>{{$current_user->university}}</h6>
                        <h6>{{$current_user->specialization}}</h6>
                    </div>
                    <!-- List group -->
                    <ul class="list-group">
                        <a href="/friends" class="list-group-item"><i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;"></i>
                            <span class="badge">{{$current_user->friends->count()}}</span>
                            Friends
                        </a>
                        <a href="/groups" class="list-group-item"><i class="fa fa-users" aria-hidden="true" style="margin-right: 10px;"></i>
                            <span class="badge">{{$current_user->groups->count()}}</span>
                            Groups
                        </a>
                        <a href="/rooms" class="list-group-item"><i class="fa fa-comments" aria-hidden="true" style="margin-right: 10px;"></i>
                            <span class="badge">0</span>
                            Chat Rooms
                        </a>
                    </ul>
                </div>
                <footer style="text-align: center">
                    Copyright &copy; 2017
                </footer>
            </div>
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>Account Settings</h1>
                        <hr>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-md-8">
                                    <br>
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <ul style="padding-left: 15px;">
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <form method="POST" action="{{ url('/users/update') }}" class="form-horizontal" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="PATCH">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-3 control-label">Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" value="{{$current_user->name}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="col-sm-3 control-label">Email</label>
                                            <div class="col-sm-9">
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{$current_user->email}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="university" class="col-sm-3 control-label">University</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="university" name="university" placeholder="Your University" value="{{$current_user->university}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="specialization" class="col-sm-3 control-label">Specialization</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="specialization" name="specialization" placeholder="Your Specialization" value="{{$current_user->specialization}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="avatar" class="col-sm-3 control-label">Your Avatar</label>
                                            <div class="col-sm-9">
                                                <input type="file" id="avatar" name="avatar" accept="image/jpeg">
                                                <p class="help-block">Only JPG file</p>
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-9">
                                                <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
