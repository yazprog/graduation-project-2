@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- USER -->
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align: center">
                        <div class="col-md-4">
                            <img src="{{ \Illuminate\Support\Facades\Storage::disk('s3')->url($current_user->avatar_name) }}" alt="profile image" class="img-circle" width="100%" style="border: 2px solid #CCC;margin: 5px auto;">
                        </div>
                        <div class="col-md-8">
                            <h5>{{$current_user->name}}</h5>
                            <h6>{{$current_user->specialization}}</h6>
                        </div>
                    </div>
                </div>
                <!-- NEW GROUP -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4><b>New Room</b></h4>
                        <hr>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul style="padding-left: 15px;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form" rele="form" method="POST" action="{{ url('/rooms')  }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="room_name">Room Name</label>
                                <input type="text" class="form-control" id="group_name" name="room_name" required>
                            </div>
                            <div class="form-group">
                                <label for="group_id">Choose Group</label>
                                <select class="form-control" name="group_id" id="group_id">
                                    <option disabled selected>Your Groups</option>
                                    @forelse($current_user->groups as $group)
                                        <option value="{{$group->id}}">{{$group->name}}</option>
                                    @empty
                                        <option selected disabled>No Groups</option>
                                    @endforelse
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-default btn-block btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4><b>Invitations to me</b></h4>
                    </div>
                    <!-- List group -->
                    <ul class="list-group">
                        @if(!\App\Invitation::where('receiver_id',$current_user->id)->where('state',false)->get()->isEmpty())
                        @foreach(\App\Invitation::where('receiver_id',$current_user->id)->where('state','=',false)->get() as $invitation)
                            <li class="list-group-item">
                                <form method="POST" action="{{ url('/rooms/invitations/accept') }}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="invitation_id" value="{{$invitation->id}}">
                                    <button class="btn btn-xs btn-primary pull-right" type="submit">Accept</button>
                                </form>
                                {{\App\Room::find($invitation->room_id)->name}}
                            </li>
                        @endforeach
                        @else
                            <li class="list-group-item">No Invitations</li>
                        @endif
                    </ul>
                </div>
                <footer style="text-align: center">
                    Copyright &copy; 2017
                </footer>
            </div>
            <!--------------------------------------->
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>My Rooms <small>Subtext for header</small></h1>
                        <hr>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#my" aria-controls="my" role="tab" data-toggle="tab">My Rooms</a></li>
                                    <li role="presentation"><a href="#other" aria-controls="other" role="tab" data-toggle="tab">Other Rooms</a></li>
                                </ul>
                                <br>
                                <div class="tab-content">
                                    <!-- MY -->
                                    <div role="tabpanel" class="tab-pane active" id="my">
                                        @if(!$current_user->rooms->isEmpty())
                                            @foreach($current_user->rooms as $room)
                                                <div class="col-md-4">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body" style="text-align: center">
                                                            <h4 class="text-left"><a href="/rooms/{{$room->id}}">{{$room->name}}</a></h4>
                                                            <h5 class="text-left"><b>Group : </b>{{\App\Group::find($room->group_id)->name}}</h5>
                                                            <hr>
                                                            <div class="pull-left">
                                                                <form class="form-inline" role="form" method="post" action="{{ url('/rooms/'.$room->id) }}" style="display: inline-block;">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="_method" value="DELETE">
                                                                    <button class="btn btn-xs btn-danger" type="submit">Remove</button>
                                                                </form>
                                                                <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#editGroupModal{{$room->id}}">Edit</button>
                                                            </div>
                                                            <div class="pull-right">
                                                                <span class="badge"><i class="fa fa-user" aria-hidden="true"></i> <span style="margin-left: 5px;">{{\App\Group::find($room->group_id)->members->count()}}</span> </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Edit Modal -->
                                                <div class="modal fade" id="editGroupModal{{$room->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title" id="myModalLabel">Edit Room</h4>
                                                            </div>
                                                            <form method="POST" action="{{ url('/rooms/'.$room->id ) }}" class="form-horizontal">
                                                                <div class="modal-body">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="_method" value="PATCH">
                                                                    <div class="form-group">
                                                                        <label for="room_name" class="col-sm-3 control-label">Room Name</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="form-control" id="room_name" name="room_name" placeholder="Room Name" value="{{$room->name}}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="group_id" class="col-sm-3 control-label">Group Name</label>
                                                                        <div class="col-sm-9">
                                                                            <select class="form-control" name="group_id" id="group_id">
                                                                                <option disabled selected>Your Groups</option>
                                                                                @foreach($current_user->groups as $group)
                                                                                    <option value="{{$group->id}}">{{$group->name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                                         </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-default">Save</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <h1>No Rooms</h1>
                                        @endif
                                    </div>
                                    <!-- OTHERS -->
                                    <div role="tabpanel" class="tab-pane" id="other">
                                        @forelse(\App\Invitation::where('receiver_id',$current_user->id)->where('state',true)->get() as $invitation)
                                            <div class="col-md-4">
                                                <div class="panel panel-default">
                                                    <div class="panel-body" style="text-align: center">
                                                        <h4 class="text-left"><a href="/rooms/{{\App\Room::find($invitation->room_id)->id}}">{{\App\Room::find($invitation->room_id)->name}}</a></h4>
                                                        <h5 class="text-left"><b>Group : </b>{{\App\Group::find(\App\Room::find($invitation->room_id)->group_id)->name}}</h5>
                                                        <hr>
                                                        <div class="pull-right">
                                                            <span class="badge"><i class="fa fa-user" aria-hidden="true"></i> <span style="margin-left: 5px;">{{\App\Group::find(\App\Room::find($invitation->room_id)->group_id)->members->count()}}</span> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @empty
                                            <h1>No Rooms</h1>
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
