<!-- Tab panes -->
<div class="tab-content">
    <!-- -->
    <div role="tabpanel" class="tab-pane active" id="downloads">
        <br>
        <ul class="list-group">
            @foreach(\App\Question::all() as $question)
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-md-8">
                            <a href="/files/{{$file->id}}" style="font-size: 20px;">{{$file->original_file_name}}</a>
                            <p>{{$file->description}}</p>
                            <span class="label label-default">{{\App\User::find($file->user_id)->name}}</span>
                            @if($file->mime == 'application/pdf')
                                <span class="label label-danger">PDF</span>
                            @elseif($file->mime == 'application/msword')
                                <span class="label label-primary">WORD</span>
                            @elseif($file->mime == 'image/png' || $file->mime == 'image/jpeg')
                                <span class="label label-warning">IMAGE</span>
                            @endif
                            <small style="margin-left: 7px;">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $file->created_at)->diffForHumans()  }}</small>
                        </div>
                        <div class="col-md-2">
                            <div class="panel panel-default" style="text-align: center">
                                <div class="panel-body">
                                    <i class="fa fa-download fa-lg" aria-hidden="true"></i>
                                    <h4 style="margin: 2px;">{{$file->downloads}}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="panel panel-default" style="text-align: center">
                                <div class="panel-body">
                                    <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                    <h4 style="margin: 2px;">{{$file->views}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
    <!-- -->
    <div role="tabpanel" class="tab-pane" id="views">
        <br>
        <ul class="list-group">
            @foreach(\App\Sfile::all()->sortByDesc('views') as $file)
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-md-8">
                            <a href="/files/{{$file->id}}" style="font-size: 20px;">{{$file->original_file_name}}</a>
                            <p>{{$file->description}}</p>
                            <span class="label label-default">{{\App\User::find($file->user_id)->name}}</span>
                            @if($file->mime == 'application/pdf')
                                <span class="label label-danger">PDF</span>
                            @elseif($file->mime == 'application/msword')
                                <span class="label label-primary">WORD</span>
                            @elseif($file->mime == 'image/png' || $file->mime == 'image/jpeg')
                                <span class="label label-warning">IMAGE</span>
                            @endif
                            <small style="margin-left: 7px;">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $file->created_at)->diffForHumans()  }}</small>
                        </div>
                        <div class="col-md-2">
                            <div class="panel panel-default" style="text-align: center">
                                <div class="panel-body">
                                    <i class="fa fa-download fa-lg" aria-hidden="true"></i>
                                    <h4 style="margin: 2px;">{{$file->downloads}}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="panel panel-default" style="text-align: center">
                                <div class="panel-body">
                                    <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                    <h4 style="margin: 2px;">{{$file->views}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
    <!-- -->
    <div role="tabpanel" class="tab-pane" id="week">
        <br>
        <ul class="list-group">
            @foreach(\App\Sfile::where('created_at','>=',\Carbon\Carbon::now()->subWeek()->toDateTimeString())->get()->sortByDesc('created_at') as $file)
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-md-8">
                            <a href="/files/{{$file->id}}" style="font-size: 20px;">{{$file->original_file_name}}</a>
                            <p>{{$file->description}}</p>
                            <span class="label label-default">{{\App\User::find($file->user_id)->name}}</span>
                            @if($file->mime == 'application/pdf')
                                <span class="label label-danger">PDF</span>
                            @elseif($file->mime == 'application/msword')
                                <span class="label label-primary">WORD</span>
                            @elseif($file->mime == 'image/png' || $file->mime == 'image/jpeg')
                                <span class="label label-warning">IMAGE</span>
                            @endif
                            <small style="margin-left: 7px;">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $file->created_at)->diffForHumans()  }}</small>
                        </div>
                        <div class="col-md-2">
                            <div class="panel panel-default" style="text-align: center">
                                <div class="panel-body">
                                    <i class="fa fa-download fa-lg" aria-hidden="true"></i>
                                    <h4 style="margin: 2px;">{{$file->downloads}}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="panel panel-default" style="text-align: center">
                                <div class="panel-body">
                                    <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                    <h4 style="margin: 2px;">{{$file->views}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
    <!-- -->
    <div role="tabpanel" class="tab-pane" id="month">
        <br>
        <ul class="list-group">
            @foreach(\App\Sfile::where('created_at','>=',\Carbon\Carbon::now()->subMonth()->toDateTimeString())->get()->sortByDesc('created_at') as $file)
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-md-8">
                            <a href="/files/{{$file->id}}" style="font-size: 20px;">{{$file->original_file_name}}</a>
                            <p>{{$file->description}}</p>
                            <span class="label label-default">{{\App\User::find($file->user_id)->name}}</span>
                            @if($file->mime == 'application/pdf')
                                <span class="label label-danger">PDF</span>
                            @elseif($file->mime == 'application/msword')
                                <span class="label label-primary">WORD</span>
                            @elseif($file->mime == 'image/png' || $file->mime == 'image/jpeg')
                                <span class="label label-warning">IMAGE</span>
                            @endif
                            <small style="margin-left: 7px;">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $file->created_at)->diffForHumans()  }}</small>
                        </div>
                        <div class="col-md-2">
                            <div class="panel panel-default" style="text-align: center">
                                <div class="panel-body">
                                    <i class="fa fa-download fa-lg" aria-hidden="true"></i>
                                    <h4 style="margin: 2px;">{{$file->downloads}}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="panel panel-default" style="text-align: center">
                                <div class="panel-body">
                                    <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                    <h4 style="margin: 2px;">{{$file->views}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>