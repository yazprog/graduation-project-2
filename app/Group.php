<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','user_id'
    ];

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members(){
        return $this->belongsToMany('App\User','users_groups','group_id','user_id');
    }

    /**
     *
     *
     * @param $id
     */
    public function addMember($id){
        $this->members()->attach($id);
        return;
    }

    /**
     *
     *
     * @param $id
     */
    public function removeMember($id){
        $this->members()->detach($id);
        return;
    }

    public function rooms(){
        return $this->hasMany('App\Room');
    }

}
