<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

use Auth;
use Symfony\Component\VarDumper\Cloner\Data;

class Sfile extends Model
{
    //
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'files';


    public function getCreatedAtAttribute($timestamp) {
         $time = strtotime($timestamp);
         return date('Y-m-d H:i:s',$time);
    }
}
