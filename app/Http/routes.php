<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\App;

Route::get('/', function () {
    return redirect('/home');
});

Route::auth();

// Home Page After Logged in
Route::get('/home', 'HomeController@index');
// User Profile
Route::get('/me','HomeController@profile');
// Other User Profile
Route::get('/profile/{id}','HomeController@other_profile');
// Settings Page
Route::get('/settings','HomeController@settings');
// Update User Info
Route::patch('/users/update','HomeController@update');
//
Route::post('/users/search','HomeController@search');

/*********************************/
// [ Friend ] Operation Routes
// [ Author : Yazeed ]
/*********************************/

// Friend Route
Route::resource('friends', 'FriendController' , ['only' => [
    'index', 'store' , 'destroy'
]]);

/*********************************/
// [ Group ] Operation Routes
// [ Author : Yazeed ]
/*********************************/

// Group Route
Route::resource('groups', 'GroupController' , ['only' => [
    'index' ,'show' ,'store' , 'update' , 'destroy'
]]);

// Add Member to Group
Route::post('/groups/member','GroupController@addMember');

// Remove Member from Group
Route::delete('/groups/member/{id}','GroupController@removeMember');

/*********************************/
// [ File ] Operation Routes
// [ Author : Yazeed ]
/*********************************/

// File Route
Route::resource('files', 'FileController' , ['only' => [
    'index', 'show' , 'store' , 'update' ,'destroy'
]]);

Route::post('/files/download/{id}','FileController@download');

Route::post('/files/search','FileController@search');

/*********************************/
// [ Question ] Operation Routes
// [ Author : Yazeed ]
/*********************************/

// Question Route
Route::resource('questions', 'QuestionController' , ['only' => [
    'index', 'show' , 'store' , 'update' ,'destroy'
]]);

Route::post('/questions/search','QuestionController@search');

/*********************************/
// [ Answer ] Operation Routes
// [ Author : Yazeed ]
/*********************************/

// Answer Route
Route::resource('answers', 'AnswerController' , ['only' => [
    'store' ,'destroy'
]]);

/*********************************/
// [ Rooms ] Operation Routes
// [ Author : Yazeed ]
/*********************************/

// Rooms Route
Route::resource('rooms', 'RoomsController' , ['only' => [
    'index', 'show' , 'store' , 'update' ,'destroy'
]]);

Route::post('/rooms/invitations','RoomsController@sendInvitations');
Route::post('/rooms/invitations/accept','RoomsController@acceptInvitations');
Route::post('/rooms/chat/message','RoomsController@postMessage');