<?php

namespace App\Http\Controllers;

use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Group;

use Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class GroupController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $current_user = Auth::user();
        return view('groups')
            ->with('current_user',$current_user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'icon' => 'required|file|mimes:jpeg,jpg,jpe,png|dimensions:max_width=200,max_height=200',
            'group_name' => 'required|min:10',
        ]);

        if ($validator->fails()) {
            return redirect('/groups')
                ->withErrors($validator);
        }

        $exist_group = Group::where('name','=',$request->group_name)->get();
        if($exist_group->isEmpty()){
            $user = Auth::user();
            $icon_name = 'group-icon.jpg';

            if(isset($request->icon)){
                $icon = $request->icon;
                $ext = $icon->getClientOriginalExtension();
                $icon_name = time().''.Auth::user()->id.'-group-icon.'.$ext;
                Storage::disk('s3')->put($icon_name,File::get($icon));
            }

            //
            $new_group = new Group;
            $new_group->user_id = $user->id;
            $new_group->name = $request->group_name;
            $new_group->icon_name = $icon_name;
            $new_group->save();
            //
            return back();
        }else{
            return back()->withErrors('This group already exist !');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $group = Group::find($id);
        $current_user = Auth::user();
        if(!is_null($group)){
            return view('group')
                    ->with('group',$group)
                    ->with('current_user',$current_user);
        }else{
            return redirect('/me');
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'group_name' => 'required|min:10',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator);
        }

        //
        $group = Group::find($id);
        $user = Auth::user();
        $icon_name = $group->icon_name;

        if(isset($request->icon)){
            $icon = $request->icon;
            $ext = $icon->getClientOriginalExtension();
            $icon_name = time().''.$user->id.'-group-icon.'.$ext;
            Storage::disk('s3')->put($icon_name,File::get($icon));
        }

        $group->name = $request->group_name;
        $group->icon_name = $icon_name;
        $group->save();
        //
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $group = Group::find($id);
        //
        foreach ($group->members as $member){
            $group->removeMember($member->id);
        }
        // New Additions are not assigned to the report
        foreach ($group->rooms as $room){
            foreach ($room->invitations as $invitation){
                $invitation->delete();
            }
            $room->delete();
        }
        $group->delete();
        return back();
    }


    public function addMember(Request $request){
        $group = Group::find($request->group_id);
        if(!is_null($group)){
            $member = $group->members()->where('user_id','=',$request->member_id)->get();
            if($member->isEmpty()){
                $group->addMember($request->member_id);
                return back();
            }else{
                return back();
            }
        }else{
            return back();
        }
    }

    public function removeMember(Request $request,$id){
        $group = Group::find($request->group_id);
        if(!is_null($group)){
            $group->removeMember($id);
            return back();
        }else{
            return back();
        }
    }


}
