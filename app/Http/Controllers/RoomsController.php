<?php

namespace App\Http\Controllers;

use Event;
use App\Events\NotifyUser;
use App\Group;
use App\Invitation;
use Illuminate\Support\Facades\App;
use Validator;
use App\Room;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class RoomsController extends Controller
{
    var $pusher;
    var $user;
    var $chatChannel;
    const DEFAULT_CHAT_CHANNEL = 'chat';

    /**
     * Create a new controller instance.
     *
     *
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
        //
        $this->pusher = App::make('pusher');
        $this->user = Auth::user();
        $this->chatChannel = self::DEFAULT_CHAT_CHANNEL;
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        //
        $current_user = Auth::user();
        return view('rooms')
            ->with('current_user',$current_user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'room_name' => 'required|min:2|max:20',
            'group_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/rooms')
                ->withErrors($validator);
        }

        $exist_room = Room::where('name','=',$request->room_name)->get();
        //
        if($exist_room->isEmpty()){
            $new_room = new Room;
            $new_room->user_id = Auth::user()->id;
            $new_room->group_id = $request->group_id;
            $new_room->name = $request->room_name;
            $new_room->save();
            return back();
        }else{
            return back()->withErrors('Make sure are you updating available room !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $current_user = Auth::user();
        $room = Room::find($id);

        //
        if($room->user_id != $current_user->id){
            $invitations = Invitation::where('room_id',$room->id)
                                     ->where('receiver_id',$current_user->id)
                                     ->where('state','=',true)->get();
            if($invitations->isEmpty()){
                return view('errors.404');
            }
        }

        return view('room')
            ->withRoom($room)
            ->with('current_user',$current_user)
            ->with('chatChannel',$this->chatChannel);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $room = Room::find($id);
        $room->name = $request->room_name;
        $room->group_id = $request->group_id;
        $room->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $room = Room::find($id);
        $room->delete();
        return back();
    }

    /**
     *
     *
     * @param Request $request
     */
    public function postMessage(Request $request)
    {
        $message = [
            'text' => e($request->chat_text),
            'name' => $this->user->name,
            'avatar' => $this->user->avatar_name,
            'timestamp' => (time()*1000)
        ];
        $this->pusher->trigger($this->chatChannel, 'new-message', $message);
    }

    public function sendInvitations(Request $request){
        $group = Group::find($request->group_id);
        if(!is_null($group)){
            foreach($group->members as $member){
                if(Invitation::where('receiver_id','=',$member->id)->where('room_id','=',$request->room_id)->get()->isEmpty()){
                    $new_invitation = new Invitation;
                    $new_invitation->user_id = Auth::user()->id;
                    $new_invitation->receiver_id = $member->id;
                    $new_invitation->room_id = $request->room_id;
                    $new_invitation->state = 'Invited';
                    $new_invitation->save();
                    // Send Notification
                    Event::fire(new NotifyUser(['user_id' => $member->id ,'msg' => 'You Got Invitation from '.Auth::user()->name]));
                }
            }
        }else{
            return back()->withErrors('There is no members in this group !');
        }
        return back();
    }

    public function acceptInvitations(Request $request){
        $invitation = Invitation::find($request->invitation_id);
        $invitation->state = true;
        $invitation->save();
        return back();
    }
}

