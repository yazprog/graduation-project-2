<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Question;
use App\Sfile;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current_user = Auth::user();
        $week_ago = Carbon::now()->subWeek()->toDateTimeString();

        //
        $my_files = Sfile::where([
            ['user_id','=',$current_user->id],
            ['created_at','>=',$week_ago]
        ])->take(10)->get();

        //
        $my_questions = Question::where([
            ['user_id','=',$current_user->id],
            ['created_at','>=',$week_ago]
        ])->take(10)->get();

        //
        $friends_files = collect();
        foreach ($current_user->friends as $friend){
            $files = Sfile::where([
                ['user_id','=',$friend->id],
                ['created_at','>=',$week_ago]
            ])->get();
            $friends_files = $friends_files->merge($files);
        }

        //
        $friends_questions = collect();
        foreach ($current_user->friends as $friend){
            $questions = Question::where([
                ['user_id','=',$friend->id],
                ['created_at','>=',$week_ago]
            ])->get();
            $friends_questions = $friends_questions->merge($questions);
        }


        return view('home')
            ->with('current_user',$current_user)
            ->with('my_files',$my_files)
            ->with('friends_files',$friends_files->take(10))
            ->with('my_questions',$my_questions)
            ->with('friends_questions',$friends_questions->take(10));
    }

    /**
     * Function Description
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile(){
        $current_user = Auth::user();
        return view('profile')
                ->with('current_user',$current_user);
    }

    /**
     * Function Description
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function other_profile($id){
        if(Auth::user()->id != $id){
            $current_user = User::find($id);
            if(!is_null($current_user)){
                $friends = Auth::user()->friends()->where('friend_id','=',$id)->get();
                $he_is_friends = false;
                if(!$friends->isEmpty()){
                    $he_is_friends = true;
                }
                return view('profile')
                    ->with('current_user',$current_user)
                    ->with('he_is_friends',$he_is_friends);
            }else{
                return back();
            }

        }else{
            return redirect('/me');
        }
    }

    /**
     * Function Description
     *
     * @return $this
     */
    public function settings(){
        $current_user = Auth::user();
        return view('settings')->with('current_user',$current_user);
    }

    public function update(Request $request){

        $user = Auth::user();
        $avatar_name = $user->avatar_name;

        if(isset($request->avatar)){
            $avatar = $request->avatar;
            $ext = $avatar->getClientOriginalExtension();
            $avatar_name = time().''.Auth::user()->id.'-avatar.'.$ext;
            Storage::disk('s3')->put($avatar_name,File::get($avatar));
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->university = $request->university;
        $user->specialization = $request->specialization;
        $user->avatar_name = $avatar_name;
        $user->save();

        return back();
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function search(Request $request){
        if(!is_null($request->name)){
            $current_user = Auth::user();
            $users = User::where('name','LIKE','%'.$request->name.'%')->get();
            if(!$users->isEmpty()){
                return view('search.users')->with('users',$users)->with('current_user',$current_user);
            }else{
                return view('search.users')->withErrors('No Result')->with('current_user',$current_user);
            }
        }else{
            return back();
        }
    }

}
