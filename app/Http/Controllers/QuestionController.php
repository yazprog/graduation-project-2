<?php

namespace App\Http\Controllers;

use Validator;

use App\Question;
use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

class QuestionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $current_user = Auth::user();
        return view('questions')
            ->with('current_user',$current_user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:100|min:10',
            'question' => 'required|min:20',
        ]);

        if ($validator->fails()) {
            return redirect('/questions')
                ->withErrors($validator);
        }

        //
        $user = Auth::user();
        $new_question = new Question;
        $new_question->user_id = $user->id;
        $new_question->title = $request->title;
        $new_question->content = $request->question;
        $new_question->save();
        //
        return redirect('/questions/'.$new_question->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $current_user = Auth::user();
        $question = Question::find($id);
        if(!is_null($question)) {
            $question->views = $question->views + 1;
            $question->save();
            return view('question')
                ->with('current_user', $current_user)
                ->with('question', $question);
        }
        else{
            return redirect('/questions');
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $question = Question::find($id);
        $user = Auth::user();
        $question->title = $request->title;
        $question->content = $request->question;
        $question->save();
        //
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $question = Question::find($id);
        foreach ($question->answers as $answer){
            $answer->delete();
        }
        $question->delete();
        return redirect('/questions');
    }

    /**
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function search(Request $request){
        if(!is_null($request->title)){
            $current_user = Auth::user();
            $questions = Question::where('title','LIKE','%'.$request->title.'%')->get();
            if(!$questions->isEmpty()){
                return view('search.question')->with('questions',$questions)->with('current_user',$current_user);
            }else{
                return view('search.question')->withErrors('No Result')->with('current_user',$current_user);
            }
        }else{
            return back();
        }
    }
}
