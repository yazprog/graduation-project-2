<?php

namespace App\Http\Controllers;

use Event;

use App\Events\NotifyUser;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use Auth;

class FriendController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $current_user = Auth::user();
        //
        $not_friends = User::where('id', '!=', Auth::user()->id)
                            ->whereNotIn('id', Auth::user()->friends->modelKeys());
        //
        $suggestions = $not_friends->take(3)->get();

        return view('friends')
            ->with('current_user',$current_user)
            ->with('suggestions',$suggestions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(Auth::user()->id != $request->friend_id){
            $friend = Auth::user()->friends()->where('friend_id','=',$request->friend_id)->get();
            if($friend->isEmpty()){
                $user = User::find($request->friend_id);
                Auth::user()->addFriend($user->id);
                Event::fire(new NotifyUser(['user_id' => $user->id ,'msg' => Auth::user()->name.' followed you !']));
                return back();
            }else{
                return back()->withErrors('This user is already followed by you !');
            }
        }else{
            return back()->withErrors('Invalid Request');
        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::find($id);
        Auth::user()->removeFriend($user->id);
        return back();
    }


}
