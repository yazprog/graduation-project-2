<?php

namespace App\Http\Controllers;

use Event;
use App\Events\NotifyUser;
use App\Question;
use Validator;
use App\Answer;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class AnswerController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'answer' => 'required|min:10',
        ]);

        if ($validator->fails()) {
            return redirect('/questions/'.$request->question_id)
                ->withErrors($validator);
        }
        //
        $user = Auth::user();
        $new_answer = new Answer;
        $new_answer->user_id = $user->id;
        $new_answer->question_id = $request->question_id;
        $new_answer->content = $request->answer;
        $new_answer->save();

        $question = Question::find($request->question_id);

        Event::fire(new NotifyUser(['user_id' => $question->user_id ,'msg' => Auth::user()->name.' answered your question !']));

        //
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $answer = Answer::find($id);
        $answer->delete();
        return back();
    }
}
