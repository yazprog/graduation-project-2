<?php

namespace App\Http\Controllers;

use Event;

use App\Events\NotifyUser;

use Illuminate\Support\Facades\DB;
use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\File;

use App\Sfile;


use Illuminate\Support\Facades\Storage;


class FileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $current_user = Auth::user();
        return view('files')
            ->with('current_user',$current_user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sfile' => 'required|file|mimes:pdf,doc,dot,jpeg,jpg,jpe,png',
            'desc' => 'required|min:10',
        ]);

        if ($validator->fails()) {
            return redirect('/files')
                ->withErrors($validator);
        }
        //
        $file = $request->sfile;
        $ext = $file->getClientOriginalExtension();
        $file_name = time().''.Auth::user()->id.'.'.$ext;
        //
        Storage::disk('s3')->put($file_name,File::get($file));
        //
        $new_file = new Sfile;
        $new_file->user_id = Auth::user()->id;
        $new_file->file_name = $file_name;
        $new_file->mime = $file->getClientMimeType();
        $new_file->original_file_name = $file->getClientOriginalName();
        $new_file->description = $request->desc;
        $new_file->save();

        $AsFriendForThem = DB::table('friends_users')->where('friend_id','=',Auth::user()->id)->get();

        if(!empty($AsFriendForThem)){
           foreach ($AsFriendForThem as $f){
               Event::fire(new NotifyUser(['user_id' => $f->user_id ,'msg' => Auth::user()->name.' uploaded New File !']));
           }
        }

        return redirect('/files/'.$new_file->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $current_user = Auth::user();
        $file = Sfile::find($id);
        if(!is_null($file)) {
            $file->views = $file->views + 1;
            $file->save();
            return view('file')
                ->with('current_user',$current_user)
                ->with('file',$file);
        }else{
            return redirect('/files');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $file = Sfile::find($id);
        $file->original_file_name = $request->file_name;
        $file->description = $request->desc;
        $file->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $file = Sfile::find($id);
        //
        Storage::disk('s3')->delete($file->file_name);
        //
        $file->delete();
        return redirect('/files');
    }


    public function download($id){
        $file = Sfile::find($id);
        if(!is_null($file)){
            $exists = Storage::disk('s3')->exists($file->file_name);
            if($exists){

                $url = Storage::url($file->file_name);
                $file->downloads = $file->downloads + 1;
                $file->save();
                return redirect($url);
            }else{
                return back();
            }
        }else{
            return back();
        }
    }

    public function search(Request $request){
        if(!is_null($request->name)){
            $current_user = Auth::user();
            $files = Sfile::where('original_file_name','LIKE','%'.$request->name.'%')->get();
            if(!$files->isEmpty()){
                return view('search.file')->with('files',$files)->with('current_user',$current_user);
            }else{
                return view('search.file')->withErrors('No Result')->with('current_user',$current_user);
            }
        }else{
            return back();
        }
    }
}
