<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $dateFormat = 'y-m-d';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'university' , 'specialization' , 'avatar_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function friends()
    {
        return $this->belongsToMany( $this , 'friends_users', 'user_id', 'friend_id');
    }

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function friendsForThem()
    {
        return $this->belongsToMany( $this , 'friends_users', 'friend_id', 'user_id');
    }

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groups(){
        return $this->hasMany('App\Group');
    }

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files(){
        return $this->hasMany('App\Sfile');
    }

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions(){
        return $this->hasMany('App\Question');
    }

    public function rooms(){
        return $this->hasMany('App\Room');
    }

    public function messages(){
        return $this->hasMany('App\Message');
    }

    /**
     *
     *
     * @param $id
     */
    public function addFriend($id)
    {
        $this->friends()->attach($id);
        return;
    }

    /**
     *
     *
     * @param $id
     */
    public function removeFriend($id)
    {
        $this->friends()->detach($id);
        return;
    }
}
