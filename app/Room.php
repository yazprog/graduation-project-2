<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    //
    public function user(){
        return $this->belongsTo('App\User');
    }

    //
    public function group(){
        return $this->hasOne('App\Group');
    }

    public function invitations(){
        return $this->hasMany('App\Invitation');
    }

}
